class CreateProjects < ActiveRecord::Migration[5.2]
  def change
    create_table :projects do |t|
      t.string :name
      t.text :description
      t.date :release_date
      t.belongs_to :team
      t.timestamps
    end
    create_table :teams do |t|
      t.string :name
      t.timestamps
    end
    add_column :admin_users, :team_id, :integer, null: true
  end
end
